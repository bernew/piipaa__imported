# Piipaa

Python installs Python anywhere.

[License: MIT](https://git.sr.ht/~sthagen/piipaa/tree/default/item/LICENSE)

Third party dependencies are documented in the folder [third-party](third-party/README.md).

[![version](https://img.shields.io/pypi/v/piipaa.svg?style=flat)](https://pypi.python.org/pypi/piipaa/)
[![downloads](https://pepy.tech/badge/piipaa/month)](https://pepy.tech/project/piipaa)
[![wheel](https://img.shields.io/pypi/wheel/piipaa.svg?style=flat)](https://pypi.python.org/pypi/piipaa/)
[![supported-versions](https://img.shields.io/pypi/pyversions/piipaa.svg?style=flat)](https://pypi.python.org/pypi/piipaa/)
[![supported-implementations](https://img.shields.io/pypi/implementation/piipaa.svg?style=flat)](https://pypi.python.org/pypi/piipaa/)

## Bug Tracker

Any feature requests or bug reports shall go to the [todos of piipaa](https://todo.sr.ht/~sthagen/piipaa).

## Primary Source repository

The main source of `piipaa` is on a mountain in central Switzerland.
We use distributed version control (git).
There is no central hub.
Every clone can become a new source for the benefit of all.
The preferred public clones of `piipaa` are:

* [on codeberg](https://codeberg.org/sthagen/piipaa) - a democratic community-driven, non-profit software development platform operated by Codeberg e.V.
* [at sourcehut](https://git.sr.ht/~sthagen/piipaa) - a collection of tools useful for software development.

## Contributions

Please do not submit "pull requests" (I found no way to disable that "feature" on GitHub).
If you like to share small changes under the repositories license please kindly do so by sending a patchset.
You can either send such a patchset per email using [git send-email](https://git-send-email.io) or 
if you are a sourcehut user by selecting "Prepare a patchset" on the summary page of your fork at [sourcehut](https://git.sr.ht/).
